
import st_selection
import nc_interactions
import nci_residues
import nci_graph
import sel_utils


### set program paths
reduce_cmd = 'REDUCE_INSTALLATION_DIRECTORY/reduce.3.23.130521.linuxi386'    # reduce command
probe_cmd = 'PROBE_INSTALLATION_DIRECTORY/probe.2.16.130520.linuxi386'       # probe command

### set paths for files to read/write
pdb_path = 'PDB'                                            # path of the original PDB file
pdb_h_path = '.'                                            # path to save PDB with H atoms produced by reduce
probe_path = '.'                                            # path to save probe results

### set file names
pdb_filename = 'pdb1hiv.ent'                                # PDB file name
sif_file = 'pdb1hiv_h.sif'                                  # cytoscape output sif file

### select chains
sel_id = '1hiv'                                             # selection identifier

# example
chains = ['A', 'B']                                         # chains to be included in the network

#################### DO NOT CHANGE CODE BELOW ##################

### run reduce and probe
(pdb_h_filename, probe_filename)=nc_interactions.get_reduce_probe_rsl(pdb_filename,pdb_path,pdb_h_path,probe_path,reduce_cmd,probe_cmd)

### get network sif file
selection_lst = sel_utils.chains_to_sel_list(sel_id,chains)
(stsel_obj,nci_obj,nci_res,nci_graph)=nci_graph.pdb_to_sif(sel_id,pdb_h_filename,pdb_h_path,selection_lst,probe_filename,probe_path,sif_file)
