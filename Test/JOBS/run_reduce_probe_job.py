import nc_interactions

### set program paths
reduce_cmd = 'REDUCE_INSTALLATION_DIRECTORY/reduce.3.23.130521.linuxi386'    # reduce command
probe_cmd = 'PROBE_INSTALLATION_DIRECTORY/probe.2.16.130520.linuxi386'       # probe command

### set paths for files to read/write
pdb_filename = 'pdb1hiv.ent'  # PDB file name
pdb_path = 'PDB'              # path of the original PDB file
pdb_h_path = 'PDB'            # path to save PDB with H atoms produced by reduce
probe_path = '.'              # path to save probe results


#################### DO NOT CHANGE CODE BELOW ##################

### run reduce and probe
nc_interactions.get_reduce_probe_rsl(pdb_filename,pdb_path,pdb_h_path,probe_path,reduce_cmd,probe_cmd)
