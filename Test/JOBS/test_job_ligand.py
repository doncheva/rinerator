
import st_selection
import nc_interactions
import nci_residues
import nci_graph
import sel_utils
import time

### set program paths
reduce_cmd = 'REDUCE_INSTALLATION_DIRECTORY/reduce.3.23.130521.linuxi386'    # reduce command
probe_cmd = 'PROBE_INSTALLATION_DIRECTORY/probe.2.16.130520.linuxi386'       # probe command

### set paths for files to read/write
pdb_path = 'PDB'                        # path of the original PDB file
pdb_h_path = '.'                        # path to save PDB with H atoms produced by reduce
probe_path = '.'                        # path to save probe results

### set file names
pdb_filename = 'pdb1hiv.ent'	        # PDB file name
sif_file = 'pdb1hiv_h.sif'              # cytoscape output sif file

sel_id = '1hiv_lig'                     # selection identifier

component1 = ['1hiv', 'protein', [[0,'A',[' ','_',' '],[' ','_',' ']], [0,'B',[' ','_',' '],[' ','_',' ']], [0,'I',[' ','_',' '],[' ','_',' ']]]]
component2 = ['HOH', 'water', [[0,'A',['W',302,' '],['W',390,' ']], [0,'B',['W',304,' '],['W',389,' ']], [0,'I',['W',301,' '],['W',339,' ']]]]
component3 = ['NOA', 'ligand', [[0,'I',['H_NOA',1,' '],[None,None,None]]]]


components = [component1, component2, component3]

#################### DO NOT CHANGE CODE BELOW ##################

### run reduce and probe
(pdb_h_filename, probe_filename)=nc_interactions.get_reduce_probe_rsl(pdb_filename,pdb_path,pdb_h_path,probe_path,reduce_cmd,probe_cmd)

### get network sif file
(stsel_obj,nci_obj,nci_res,nci_graph)=nci_graph.pdb_to_sif(sel_id,pdb_filename,pdb_path,components,probe_filename,probe_path,sif_file)