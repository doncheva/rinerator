import st_selection
import nc_interactions
import nci_residues
import nci_graph
import sel_utils
import os
import sys

############################################################################
### Generate RINs for all files in pdb_path and save results in rin_path ###
############################################################################

### set program paths
reduce_cmd = 'REDUCE_INSTALLATION_DIRECTORY/reduce.3.23.130521.linuxi386'    # reduce command
probe_cmd = 'PROBE_INSTALLATION_DIRECTORY/probe.2.16.130520.linuxi386'       # probe command

### set paths for files to read/write
pdb_path = "PDB/"                              # path of the original PDB files (should contain only pdb files!)
rin_path = "Results/"                          # path to save the results

### select chains
chains = ['A', 'B', 'I']                       # chains to be included in the network

### select ligands
ligand1 = ['NOA','I',1]                        # ligand NOA
ligand2 = ['CAV','I',3]                        # ligand CAV
ligand3 = ['APY','I',5]                        # ligand APY
ligands = [ligand1, ligand2, ligand3]          # ligands to be included in the network


############################################################################
################################### M A I N ################################
############################################################################

for pdb_filename in os.listdir(pdb_path):
    ### set names
    sel_id = pdb_filename[:-4]    
    sif_file = sel_id + "_h.sif"
    if os.path.exists(rin_path+sif_file):
	#print rin_path+sif_file + " already exists, skipping..."
	continue

    ### run reduce and probe
    (pdb_h_filename, probe_filename)=nc_interactions.get_reduce_probe_rsl(pdb_filename,pdb_path,rin_path,rin_path,reduce_cmd,probe_cmd)

    ### get network sif file
    selection_lst = sel_utils.chains_to_sel_list(sel_id,chains)
    ligand_lst = sel_utils.ligands_to_sel_list(ligands)
    if len(ligand_lst) > 0:
	selection_lst += ligand_lst
    
    #(stsel_obj,nci_obj,nci_res,nci_graph)=
    nci_graph.pdb_to_sif(sel_id,pdb_h_filename,rin_path,selection_lst,probe_filename,rin_path,sif_file)
    os.remove(rin_path+probe_filename)
