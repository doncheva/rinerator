
import st_selection
import nc_interactions
import nci_residues
import nci_graph


pdb_path = 'PDB'                        # path of PDB with H atoms produced by reduce
pdb_filename = 'pdb1hiv_h.ent'          # PDB file with H atoms
probe_path = '.'                        # path of probe file
probe_filename = 'pdb1hiv_h.probe'      # the probe file name
sif_file = 'pdb1hiv_h.sif'              # cytoscape output sif file


sel_id = '1hiv'                         # selection identifier
component1 = ['1hiv', 'protein', [[0,'A',[' ','_',' '],[' ','_',' ']]]]
#component2 = ['NOA', 'ligand', [[0,'I',['H_NOA',1,' '],[None,None,None]]]]
#component1 = ['1hiv', 'protein', [[0,'A',[' ',2,' '],[' ',57,' ']]]]
#component1 = ['1hiv', 'protein', [[0,'A',[' ',2,' '],[' ',57,' ']], [0,'A',[' ',65,' '],[' ',90,' ']]]]
#component1 = ['1hiv', 'protein', [[0,'B',[' ',25,' '],[None,None,None]],[0,'B',[' ',27,' '],[None,None,None]]]]
#component1 = ['1hiv', 'protein', [[0,'A',[' ',2,' '],[' ', 57,' ']], [0,'A',[' ',65,' '],[' ',90,' ']], [0,'B',[' ',65,' '],[' ',90,' ']]]]
#component1 = ['1hiv', 'protein', [[0,'A',[' ',20,' '],[' ',30,' ']], [0,'B',[' ',25,' '],[None,None,None]], [0,'B',[' ',26,' '],[None,None,None]], [0,'B',[' ',65,' '],[' ',68,' ']]]]

components = [component1]               # list of components
#components = [component1, component2]   # list of components

#################### DO NOT CHANGE CODE BELOW ##################


### get network sif file
(stsel_obj,nci_obj,nci_res,nci_graph)=nci_graph.pdb_to_sif(sel_id,pdb_filename,pdb_path,components,probe_filename,probe_path,sif_file)
